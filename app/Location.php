<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{

    protected $primaryKey = 'location_id';
    public $incrementing = false;

    public function recordings(){
        return $this->hasMany('App\WeatherRecordings');
    }

}
