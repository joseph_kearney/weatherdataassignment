<?php
namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Location;



class LocationController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }
    
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $allLocations = Location::all();
        return response()->json(['success' => $allLocations]);

    }


}
