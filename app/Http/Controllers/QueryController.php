<?php
namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Location;
use App\WeatherRecording;



class QueryController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }
    

    /**
     * Get the weather for a specific dateTime & place
     *
     * @param  string $dateTime
     * @param  string $locations
     * @return json 
     */
    public function weatherInPast($dateTime,$locations)
    {
        $locationData = Location::whereIn('location_name',json_decode($locations))->pluck('location_id');
        
        $labels = json_decode($locations);

        $windSpeed = array();
        $gusts = array();
        $windDirection = array();
        $pressure = array();

        $weatherRecords = WeatherRecording::where('time',$dateTime)->whereIn('location_id',$locationData)->get();
        foreach($weatherRecords as $record){
            array_push($windSpeed, $record->wind_speed); 
            array_push($gusts, $record->gust); 
            array_push($windDirection, $record->wind_direction); 
            array_push($pressure, $record->atmospheric_pressure); 
        }

        $data = [
            ['data' => $windSpeed , 'label' => 'wind speed'],
            ['data' => $gusts , 'label' => 'gusts'],
            ['data' => $windDirection , 'label' => 'wind direction'],
            ['data' => $pressure , 'label' => 'pressure']
        ];
        
        return response()->json([
            'labels' => $labels,
            'data' => $data
        ]);
    }




    /**
     * Get the list of times for a selected date
     *
     * @param  string $dateSelected
     * @return json 
     */
    public function getTimes($dateSelected){

        $dateTimes = WeatherRecording::where('time','like',$dateSelected.'%')->distinct()->pluck('time');
        $times = [];

        foreach($dateTimes as $time){
            array_push($times, substr($time, 11, 18));
        }
        return response()->json($times);
    }

    /**
     * Get the list of locations for a specific dateTime
     *
     * @param  string $dateTimeSelected
     * @return json 
     */
    public function getLocations($dateTimeSelected){
        $locations = array();
        $weatherRecords = WeatherRecording::with('locations')->where('time',$dateTimeSelected)->get();

        foreach ($weatherRecords as $record) {
            array_push($locations, $record->locations->location_name);
        }

        return response()->json($locations);
    }

}
