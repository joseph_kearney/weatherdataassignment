<?php
namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\WeatherRecording;



class WeatherRecordingController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }
    
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $allWeatherRecordings = WeatherRecording::take(5)->get();
        return response()->json(['success' => $allWeatherRecordings]);

    }


}
