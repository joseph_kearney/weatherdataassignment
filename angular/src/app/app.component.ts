import { Component } from '@angular/core';
import { WeatherDataService } from './weather-data.service';
import {IMyDpOptions, IMyDateModel} from 'mydatepicker';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private weatherDataService: WeatherDataService){}

  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true,
    animation: false
  };

  public updatedGraphData = true;
  public barChartLabels:string[] = ['','','',''];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;
  public times = [];
  public locations = [];
  public selectedTime;
  public selectedDate;
  public selectedLocations = [];
 
  public barChartData:any[] = [
    {data: [], label: ''},
    {data: [], label: ''},
    {data: [], label: ''},
    {data: [], label: ''}
  ];


  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'yyyy-mm-dd',
  };



  public onDateChanged(event: IMyDateModel) {
    this.selectedDate = event.formatted;

    this.weatherDataService.getTimes(event.formatted).subscribe(response => {
      this.times = response;
    });
  }


  public onTimeChanged(){
    var dateTime = this.selectedDate +' '+ this.selectedTime;

    this.weatherDataService.getLocations(dateTime).subscribe(response => {
      this.locations = response;
      
    });
  }


  public onLocationSelected(location){

    //update locations array
    var index = this.selectedLocations.indexOf(location);
    if (index > -1) {
        this.selectedLocations.splice(index, 1);
    }else{
      this.selectedLocations.push(location)
    }

    var dateTime = this.selectedDate +' '+ this.selectedTime;
    var encodedLocations = encodeURIComponent(JSON.stringify(this.selectedLocations));

    this.weatherDataService.getPastWeather(dateTime, encodedLocations).subscribe(response => {
      this.updateBarGraph(response);
    });
  }


  public updateBarGraph(response){

      this.barChartData = response.data;
      
      //run this to update the labels
      this.barChartLabels.length = 0;
      for (let i = response.labels.length - 1; i >= 0; i--) {
        this.barChartLabels.unshift(response.labels[i]);
      }
  }

  public chartClicked(){
    //placeholder to stop chart click error
  }

}
