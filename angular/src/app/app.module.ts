import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts';
import { HttpModule } from '@angular/http';
import { MyDatePickerModule } from 'mydatepicker';
import { AppComponent } from './app.component';
import { WeatherDataService } from './weather-data.service';
import {MatButtonModule, MatCheckboxModule, MatOptionModule, MatSelect, MatSelectModule, MatRadioModule} from '@angular/material';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ChartsModule,
    HttpModule,
    MyDatePickerModule,
    MatButtonModule,
    MatOptionModule,
    MatCheckboxModule,
    MatSelectModule,
    MatRadioModule,
  ],
  providers: [
    WeatherDataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
