import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response, Headers } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import 'rxjs/add/operator/map';


@Injectable()
export class WeatherDataService {

  constructor(private http: Http) { }

  private apiUrl:string = 'http://localhost:8888/weatherdataassignment/public/api';
  private headers = new Headers({ 'Content-Type': 'application/json' });


  public getPastWeather(dateTime,locations): Observable<any> {
    return this.http
        .get(`${this.apiUrl}/weather-in-past/`+dateTime + '/locations/' + locations, { headers: this.headers })
        .map(response => {
          return response.json();
        })
  }

  public getTimes(date): Observable<any> {
    return this.http
        .get(`${this.apiUrl}/times/`+date, { headers: this.headers })
        .map(response => {
          return response.json();
        })
  }

  public getLocations(dateTime): Observable<any> {
    return this.http
        .get(`${this.apiUrl}/locations-available/`+dateTime, { headers: this.headers })
        .map(response => {
          return response.json();
        })
  }

}

