<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resources([
    'locations' => 'LocationController',
]);

Route::resources([
    'weather-recordings' => 'WeatherRecordingController',
]);

//get the times available for the selected date
Route::get('/times/{date}', 'QueryController@getTimes');

//get the locations available for the selected datetime
Route::get('/locations-available/{date}', 'QueryController@getLocations');

//bar chart to show the weather at selected locations at a specific date & time in the past
Route::get('/weather-in-past/{dateTime}/locations/{locations}', 'QueryController@weatherInPast');

//bar chart to display average weather data for a specific date & time & place - useful for weather forecasting
Route::get('/weather-forecast/{dateTime}/locations/{locations}', 'QueryController@weatherForecast');

//line chart to display weather data
Route::get('/weather-averages', 'QueryController@weatherAverages');
